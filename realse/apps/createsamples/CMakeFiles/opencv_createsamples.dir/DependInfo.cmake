# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/oscar/Downloads/opencv-master/apps/createsamples/createsamples.cpp" "/home/oscar/Downloads/opencv-master/realse/apps/createsamples/CMakeFiles/opencv_createsamples.dir/createsamples.cpp.o"
  "/home/oscar/Downloads/opencv-master/apps/createsamples/utility.cpp" "/home/oscar/Downloads/opencv-master/realse/apps/createsamples/CMakeFiles/opencv_createsamples.dir/utility.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "__OPENCV_BUILD=1"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/oscar/Downloads/opencv-master/realse/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/objdetect/CMakeFiles/opencv_objdetect.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/calib3d/CMakeFiles/opencv_calib3d.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/features2d/CMakeFiles/opencv_features2d.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/videoio/CMakeFiles/opencv_videoio.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/ml/CMakeFiles/opencv_ml.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/flann/CMakeFiles/opencv_flann.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../3rdparty/ippicv/unpack/ippicv_lnx/include"
  "."
  "../apps/createsamples/PRIVATE"
  "../apps/createsamples"
  "../include/opencv"
  "../modules/core/include"
  "../modules/imgproc/include"
  "../modules/objdetect/include"
  "../modules/ml/include"
  "../modules/imgcodecs/include"
  "../modules/videoio/include"
  "../modules/highgui/include"
  "../modules/calib3d/include"
  "../modules/flann/include"
  "../modules/features2d/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
