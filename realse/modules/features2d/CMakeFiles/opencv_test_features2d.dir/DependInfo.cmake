# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/oscar/Downloads/opencv-master/modules/features2d/test/ocl/test_brute_force_matcher.cpp" "/home/oscar/Downloads/opencv-master/realse/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/ocl/test_brute_force_matcher.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/features2d/test/test_agast.cpp" "/home/oscar/Downloads/opencv-master/realse/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_agast.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/features2d/test/test_brisk.cpp" "/home/oscar/Downloads/opencv-master/realse/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_brisk.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/features2d/test/test_descriptors_regression.cpp" "/home/oscar/Downloads/opencv-master/realse/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_descriptors_regression.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/features2d/test/test_detectors_regression.cpp" "/home/oscar/Downloads/opencv-master/realse/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_detectors_regression.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/features2d/test/test_fast.cpp" "/home/oscar/Downloads/opencv-master/realse/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_fast.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/features2d/test/test_keypoints.cpp" "/home/oscar/Downloads/opencv-master/realse/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_keypoints.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/features2d/test/test_main.cpp" "/home/oscar/Downloads/opencv-master/realse/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_main.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/features2d/test/test_matchers_algorithmic.cpp" "/home/oscar/Downloads/opencv-master/realse/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_matchers_algorithmic.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/features2d/test/test_mser.cpp" "/home/oscar/Downloads/opencv-master/realse/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_mser.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/features2d/test/test_nearestneighbors.cpp" "/home/oscar/Downloads/opencv-master/realse/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_nearestneighbors.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/features2d/test/test_orb.cpp" "/home/oscar/Downloads/opencv-master/realse/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_orb.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/features2d/test/test_rotation_and_scale_invariance.cpp" "/home/oscar/Downloads/opencv-master/realse/modules/features2d/CMakeFiles/opencv_test_features2d.dir/test/test_rotation_and_scale_invariance.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "__OPENCV_BUILD=1"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/oscar/Downloads/opencv-master/realse/modules/ts/CMakeFiles/opencv_ts.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/features2d/CMakeFiles/opencv_features2d.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/videoio/CMakeFiles/opencv_videoio.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/flann/CMakeFiles/opencv_flann.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/ml/CMakeFiles/opencv_ml.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../3rdparty/ippicv/unpack/ippicv_lnx/include"
  "."
  "../modules/ts/include"
  "../modules/features2d/include"
  "../modules/imgcodecs/include"
  "../modules/videoio/include"
  "../modules/core/include"
  "../modules/flann/include"
  "../modules/imgproc/include"
  "../modules/ml/include"
  "../modules/highgui/include"
  "../modules/features2d/test"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
