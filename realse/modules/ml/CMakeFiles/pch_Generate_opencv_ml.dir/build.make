# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.0

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/oscar/Downloads/opencv-master

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/oscar/Downloads/opencv-master/realse

# Utility rule file for pch_Generate_opencv_ml.

# Include the progress variables for this target.
include modules/ml/CMakeFiles/pch_Generate_opencv_ml.dir/progress.make

modules/ml/CMakeFiles/pch_Generate_opencv_ml: modules/ml/precomp.hpp.gch/opencv_ml_RELEASE.gch

modules/ml/precomp.hpp.gch/opencv_ml_RELEASE.gch: ../modules/ml/src/precomp.hpp
modules/ml/precomp.hpp.gch/opencv_ml_RELEASE.gch: modules/ml/precomp.hpp
modules/ml/precomp.hpp.gch/opencv_ml_RELEASE.gch: lib/libopencv_ml_pch_dephelp.a
	$(CMAKE_COMMAND) -E cmake_progress_report /home/oscar/Downloads/opencv-master/realse/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold "Generating precomp.hpp.gch/opencv_ml_RELEASE.gch"
	cd /home/oscar/Downloads/opencv-master/realse/modules/ml && /usr/bin/cmake -E make_directory /home/oscar/Downloads/opencv-master/realse/modules/ml/precomp.hpp.gch
	cd /home/oscar/Downloads/opencv-master/realse/modules/ml && /usr/bin/c++ -DNDEBUG -fPIC "-D__OPENCV_BUILD=1" "-DCVAPI_EXPORTS" -isystem"/home/oscar/Downloads/opencv-master/3rdparty/ippicv/unpack/ippicv_lnx/include" -isystem"/home/oscar/Downloads/opencv-master/realse" -isystem"/home/oscar/Downloads/opencv-master/3rdparty/ippicv/unpack/ippicv_lnx/include" -isystem"/home/oscar/Downloads/opencv-master/realse" -I"/home/oscar/Downloads/opencv-master/modules/ml/include" -I"/home/oscar/Downloads/opencv-master/modules/ml/src" -isystem"/home/oscar/Downloads/opencv-master/realse/modules/ml" -I"/home/oscar/Downloads/opencv-master/modules/core/include" -fsigned-char -W -Wall -Werror=return-type -Werror=non-virtual-dtor -Werror=address -Werror=sequence-point -Wformat -Werror=format-security -Wmissing-declarations -Wundef -Winit-self -Wpointer-arith -Wshadow -Wsign-promo -Wno-narrowing -Wno-delete-non-virtual-dtor -fdiagnostics-show-option -Wno-long-long -pthread -fomit-frame-pointer -msse -msse2 -mno-avx -msse3 -mno-ssse3 -mno-sse4.1 -mno-sse4.2 -ffunction-sections -fvisibility=hidden -fvisibility-inlines-hidden -DCVAPI_EXPORTS -x c++-header -o /home/oscar/Downloads/opencv-master/realse/modules/ml/precomp.hpp.gch/opencv_ml_RELEASE.gch /home/oscar/Downloads/opencv-master/realse/modules/ml/precomp.hpp

modules/ml/precomp.hpp: ../modules/ml/src/precomp.hpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/oscar/Downloads/opencv-master/realse/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold "Generating precomp.hpp"
	cd /home/oscar/Downloads/opencv-master/realse/modules/ml && /usr/bin/cmake -E copy_if_different /home/oscar/Downloads/opencv-master/modules/ml/src/precomp.hpp /home/oscar/Downloads/opencv-master/realse/modules/ml/precomp.hpp

pch_Generate_opencv_ml: modules/ml/CMakeFiles/pch_Generate_opencv_ml
pch_Generate_opencv_ml: modules/ml/precomp.hpp.gch/opencv_ml_RELEASE.gch
pch_Generate_opencv_ml: modules/ml/precomp.hpp
pch_Generate_opencv_ml: modules/ml/CMakeFiles/pch_Generate_opencv_ml.dir/build.make
.PHONY : pch_Generate_opencv_ml

# Rule to build all files generated by this target.
modules/ml/CMakeFiles/pch_Generate_opencv_ml.dir/build: pch_Generate_opencv_ml
.PHONY : modules/ml/CMakeFiles/pch_Generate_opencv_ml.dir/build

modules/ml/CMakeFiles/pch_Generate_opencv_ml.dir/clean:
	cd /home/oscar/Downloads/opencv-master/realse/modules/ml && $(CMAKE_COMMAND) -P CMakeFiles/pch_Generate_opencv_ml.dir/cmake_clean.cmake
.PHONY : modules/ml/CMakeFiles/pch_Generate_opencv_ml.dir/clean

modules/ml/CMakeFiles/pch_Generate_opencv_ml.dir/depend:
	cd /home/oscar/Downloads/opencv-master/realse && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/oscar/Downloads/opencv-master /home/oscar/Downloads/opencv-master/modules/ml /home/oscar/Downloads/opencv-master/realse /home/oscar/Downloads/opencv-master/realse/modules/ml /home/oscar/Downloads/opencv-master/realse/modules/ml/CMakeFiles/pch_Generate_opencv_ml.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : modules/ml/CMakeFiles/pch_Generate_opencv_ml.dir/depend

