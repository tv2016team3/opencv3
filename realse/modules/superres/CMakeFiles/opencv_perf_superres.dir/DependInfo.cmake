# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/oscar/Downloads/opencv-master/modules/superres/perf/perf_main.cpp" "/home/oscar/Downloads/opencv-master/realse/modules/superres/CMakeFiles/opencv_perf_superres.dir/perf/perf_main.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/superres/perf/perf_superres.cpp" "/home/oscar/Downloads/opencv-master/realse/modules/superres/CMakeFiles/opencv_perf_superres.dir/perf/perf_superres.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "__OPENCV_BUILD=1"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/oscar/Downloads/opencv-master/realse/modules/ts/CMakeFiles/opencv_ts.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/superres/CMakeFiles/opencv_superres.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/video/CMakeFiles/opencv_video.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/videoio/CMakeFiles/opencv_videoio.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/realse/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../3rdparty/ippicv/unpack/ippicv_lnx/include"
  "."
  "../modules/ts/include"
  "../modules/superres/include"
  "../modules/imgcodecs/include"
  "../modules/core/include"
  "../modules/imgproc/include"
  "../modules/video/include"
  "../modules/videoio/include"
  "../modules/highgui/include"
  "../modules/superres/perf"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
