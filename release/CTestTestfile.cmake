# CMake generated Testfile for 
# Source directory: /home/oscar/Downloads/opencv-master
# Build directory: /home/oscar/Downloads/opencv-master/release
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(3rdparty/libwebp)
subdirs(include)
subdirs(modules)
subdirs(doc)
subdirs(data)
subdirs(apps)
