# CMake generated Testfile for 
# Source directory: /home/oscar/Downloads/opencv-master/apps
# Build directory: /home/oscar/Downloads/opencv-master/release/apps
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(traincascade)
subdirs(createsamples)
subdirs(annotation)
