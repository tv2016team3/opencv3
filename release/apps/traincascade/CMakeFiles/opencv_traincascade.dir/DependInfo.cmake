# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/oscar/Downloads/opencv-master/apps/traincascade/HOGfeatures.cpp" "/home/oscar/Downloads/opencv-master/release/apps/traincascade/CMakeFiles/opencv_traincascade.dir/HOGfeatures.cpp.o"
  "/home/oscar/Downloads/opencv-master/apps/traincascade/boost.cpp" "/home/oscar/Downloads/opencv-master/release/apps/traincascade/CMakeFiles/opencv_traincascade.dir/boost.cpp.o"
  "/home/oscar/Downloads/opencv-master/apps/traincascade/cascadeclassifier.cpp" "/home/oscar/Downloads/opencv-master/release/apps/traincascade/CMakeFiles/opencv_traincascade.dir/cascadeclassifier.cpp.o"
  "/home/oscar/Downloads/opencv-master/apps/traincascade/features.cpp" "/home/oscar/Downloads/opencv-master/release/apps/traincascade/CMakeFiles/opencv_traincascade.dir/features.cpp.o"
  "/home/oscar/Downloads/opencv-master/apps/traincascade/haarfeatures.cpp" "/home/oscar/Downloads/opencv-master/release/apps/traincascade/CMakeFiles/opencv_traincascade.dir/haarfeatures.cpp.o"
  "/home/oscar/Downloads/opencv-master/apps/traincascade/imagestorage.cpp" "/home/oscar/Downloads/opencv-master/release/apps/traincascade/CMakeFiles/opencv_traincascade.dir/imagestorage.cpp.o"
  "/home/oscar/Downloads/opencv-master/apps/traincascade/lbpfeatures.cpp" "/home/oscar/Downloads/opencv-master/release/apps/traincascade/CMakeFiles/opencv_traincascade.dir/lbpfeatures.cpp.o"
  "/home/oscar/Downloads/opencv-master/apps/traincascade/old_ml_boost.cpp" "/home/oscar/Downloads/opencv-master/release/apps/traincascade/CMakeFiles/opencv_traincascade.dir/old_ml_boost.cpp.o"
  "/home/oscar/Downloads/opencv-master/apps/traincascade/old_ml_data.cpp" "/home/oscar/Downloads/opencv-master/release/apps/traincascade/CMakeFiles/opencv_traincascade.dir/old_ml_data.cpp.o"
  "/home/oscar/Downloads/opencv-master/apps/traincascade/old_ml_inner_functions.cpp" "/home/oscar/Downloads/opencv-master/release/apps/traincascade/CMakeFiles/opencv_traincascade.dir/old_ml_inner_functions.cpp.o"
  "/home/oscar/Downloads/opencv-master/apps/traincascade/old_ml_tree.cpp" "/home/oscar/Downloads/opencv-master/release/apps/traincascade/CMakeFiles/opencv_traincascade.dir/old_ml_tree.cpp.o"
  "/home/oscar/Downloads/opencv-master/apps/traincascade/traincascade.cpp" "/home/oscar/Downloads/opencv-master/release/apps/traincascade/CMakeFiles/opencv_traincascade.dir/traincascade.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "__OPENCV_BUILD=1"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/oscar/Downloads/opencv-master/release/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/objdetect/CMakeFiles/opencv_objdetect.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/calib3d/CMakeFiles/opencv_calib3d.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/features2d/CMakeFiles/opencv_features2d.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/ml/CMakeFiles/opencv_ml.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/videoio/CMakeFiles/opencv_videoio.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/flann/CMakeFiles/opencv_flann.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../3rdparty/ippicv/unpack/ippicv_lnx/include"
  "."
  "../apps/traincascade/PRIVATE"
  "../apps/traincascade"
  "../include/opencv"
  "../modules/core/include"
  "../modules/imgproc/include"
  "../modules/objdetect/include"
  "../modules/ml/include"
  "../modules/imgcodecs/include"
  "../modules/videoio/include"
  "../modules/highgui/include"
  "../modules/calib3d/include"
  "../modules/flann/include"
  "../modules/features2d/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
