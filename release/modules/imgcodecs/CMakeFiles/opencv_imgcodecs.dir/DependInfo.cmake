# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/oscar/Downloads/opencv-master/modules/imgcodecs/src/bitstrm.cpp" "/home/oscar/Downloads/opencv-master/release/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/src/bitstrm.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/imgcodecs/src/grfmt_base.cpp" "/home/oscar/Downloads/opencv-master/release/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/src/grfmt_base.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/imgcodecs/src/grfmt_bmp.cpp" "/home/oscar/Downloads/opencv-master/release/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/src/grfmt_bmp.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/imgcodecs/src/grfmt_exr.cpp" "/home/oscar/Downloads/opencv-master/release/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/src/grfmt_exr.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/imgcodecs/src/grfmt_gdal.cpp" "/home/oscar/Downloads/opencv-master/release/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/src/grfmt_gdal.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/imgcodecs/src/grfmt_hdr.cpp" "/home/oscar/Downloads/opencv-master/release/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/src/grfmt_hdr.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/imgcodecs/src/grfmt_jpeg.cpp" "/home/oscar/Downloads/opencv-master/release/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/src/grfmt_jpeg.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/imgcodecs/src/grfmt_jpeg2000.cpp" "/home/oscar/Downloads/opencv-master/release/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/src/grfmt_jpeg2000.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/imgcodecs/src/grfmt_png.cpp" "/home/oscar/Downloads/opencv-master/release/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/src/grfmt_png.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/imgcodecs/src/grfmt_pxm.cpp" "/home/oscar/Downloads/opencv-master/release/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/src/grfmt_pxm.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/imgcodecs/src/grfmt_sunras.cpp" "/home/oscar/Downloads/opencv-master/release/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/src/grfmt_sunras.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/imgcodecs/src/grfmt_tiff.cpp" "/home/oscar/Downloads/opencv-master/release/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/src/grfmt_tiff.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/imgcodecs/src/grfmt_webp.cpp" "/home/oscar/Downloads/opencv-master/release/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/src/grfmt_webp.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/imgcodecs/src/jpeg_exif.cpp" "/home/oscar/Downloads/opencv-master/release/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/src/jpeg_exif.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/imgcodecs/src/loadsave.cpp" "/home/oscar/Downloads/opencv-master/release/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/src/loadsave.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/imgcodecs/src/rgbe.cpp" "/home/oscar/Downloads/opencv-master/release/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/src/rgbe.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/imgcodecs/src/utils.cpp" "/home/oscar/Downloads/opencv-master/release/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/src/utils.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "CVAPI_EXPORTS"
  "HAVE_WEBP"
  "IMGCODECS_EXPORTS"
  "__OPENCV_BUILD=1"
  )

# Pairs of files generated by the same build rule.
set(CMAKE_MULTIPLE_OUTPUT_PAIRS
  "/home/oscar/Downloads/opencv-master/release/lib/libopencv_imgcodecs.so" "/home/oscar/Downloads/opencv-master/release/lib/libopencv_imgcodecs.so.3.1.0"
  "/home/oscar/Downloads/opencv-master/release/lib/libopencv_imgcodecs.so.3.1" "/home/oscar/Downloads/opencv-master/release/lib/libopencv_imgcodecs.so.3.1.0"
  )


# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/oscar/Downloads/opencv-master/release/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/3rdparty/libwebp/CMakeFiles/libwebp.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../3rdparty/libwebp"
  "../3rdparty/ippicv/unpack/ippicv_lnx/include"
  "."
  "/usr/include/x86_64-linux-gnu"
  "/usr/include/OpenEXR"
  "../modules/imgcodecs/include"
  "../modules/imgcodecs/src"
  "modules/imgcodecs"
  "../modules/core/include"
  "../modules/imgproc/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
