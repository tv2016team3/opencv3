# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/oscar/Downloads/opencv-master/modules/stitching/perf/opencl/perf_stitch.cpp" "/home/oscar/Downloads/opencv-master/release/modules/stitching/CMakeFiles/opencv_perf_stitching.dir/perf/opencl/perf_stitch.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/stitching/perf/opencl/perf_warpers.cpp" "/home/oscar/Downloads/opencv-master/release/modules/stitching/CMakeFiles/opencv_perf_stitching.dir/perf/opencl/perf_warpers.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/stitching/perf/perf_main.cpp" "/home/oscar/Downloads/opencv-master/release/modules/stitching/CMakeFiles/opencv_perf_stitching.dir/perf/perf_main.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/stitching/perf/perf_stich.cpp" "/home/oscar/Downloads/opencv-master/release/modules/stitching/CMakeFiles/opencv_perf_stitching.dir/perf/perf_stich.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "__OPENCV_BUILD=1"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/oscar/Downloads/opencv-master/release/modules/ts/CMakeFiles/opencv_ts.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/stitching/CMakeFiles/opencv_stitching.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/flann/CMakeFiles/opencv_flann.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/ml/CMakeFiles/opencv_ml.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/videoio/CMakeFiles/opencv_videoio.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/objdetect/CMakeFiles/opencv_objdetect.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/features2d/CMakeFiles/opencv_features2d.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/calib3d/CMakeFiles/opencv_calib3d.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../3rdparty/ippicv/unpack/ippicv_lnx/include"
  "."
  "../modules/ts/include"
  "../modules/stitching/include"
  "../modules/imgcodecs/include"
  "../modules/core/include"
  "../modules/flann/include"
  "../modules/imgproc/include"
  "../modules/ml/include"
  "../modules/videoio/include"
  "../modules/highgui/include"
  "../modules/objdetect/include"
  "../modules/features2d/include"
  "../modules/calib3d/include"
  "../modules/stitching/perf"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
