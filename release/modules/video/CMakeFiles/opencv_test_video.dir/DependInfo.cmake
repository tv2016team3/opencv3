# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/oscar/Downloads/opencv-master/modules/video/test/ocl/test_bgfg_mog2.cpp" "/home/oscar/Downloads/opencv-master/release/modules/video/CMakeFiles/opencv_test_video.dir/test/ocl/test_bgfg_mog2.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/video/test/ocl/test_optflow_farneback.cpp" "/home/oscar/Downloads/opencv-master/release/modules/video/CMakeFiles/opencv_test_video.dir/test/ocl/test_optflow_farneback.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/video/test/ocl/test_optflow_tvl1flow.cpp" "/home/oscar/Downloads/opencv-master/release/modules/video/CMakeFiles/opencv_test_video.dir/test/ocl/test_optflow_tvl1flow.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/video/test/ocl/test_optflowpyrlk.cpp" "/home/oscar/Downloads/opencv-master/release/modules/video/CMakeFiles/opencv_test_video.dir/test/ocl/test_optflowpyrlk.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/video/test/test_accum.cpp" "/home/oscar/Downloads/opencv-master/release/modules/video/CMakeFiles/opencv_test_video.dir/test/test_accum.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/video/test/test_camshift.cpp" "/home/oscar/Downloads/opencv-master/release/modules/video/CMakeFiles/opencv_test_video.dir/test/test_camshift.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/video/test/test_ecc.cpp" "/home/oscar/Downloads/opencv-master/release/modules/video/CMakeFiles/opencv_test_video.dir/test/test_ecc.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/video/test/test_estimaterigid.cpp" "/home/oscar/Downloads/opencv-master/release/modules/video/CMakeFiles/opencv_test_video.dir/test/test_estimaterigid.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/video/test/test_kalman.cpp" "/home/oscar/Downloads/opencv-master/release/modules/video/CMakeFiles/opencv_test_video.dir/test/test_kalman.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/video/test/test_main.cpp" "/home/oscar/Downloads/opencv-master/release/modules/video/CMakeFiles/opencv_test_video.dir/test/test_main.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/video/test/test_optflowpyrlk.cpp" "/home/oscar/Downloads/opencv-master/release/modules/video/CMakeFiles/opencv_test_video.dir/test/test_optflowpyrlk.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/video/test/test_tvl1optflow.cpp" "/home/oscar/Downloads/opencv-master/release/modules/video/CMakeFiles/opencv_test_video.dir/test/test_tvl1optflow.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "__OPENCV_BUILD=1"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/oscar/Downloads/opencv-master/release/modules/ts/CMakeFiles/opencv_ts.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/video/CMakeFiles/opencv_video.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/videoio/CMakeFiles/opencv_videoio.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/home/oscar/Downloads/opencv-master/release/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../3rdparty/ippicv/unpack/ippicv_lnx/include"
  "."
  "../modules/ts/include"
  "../modules/video/include"
  "../modules/imgcodecs/include"
  "../modules/videoio/include"
  "../modules/core/include"
  "../modules/imgproc/include"
  "../modules/highgui/include"
  "../modules/video/test"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
