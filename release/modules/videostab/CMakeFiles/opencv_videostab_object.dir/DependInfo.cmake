# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/oscar/Downloads/opencv-master/modules/videostab/src/deblurring.cpp" "/home/oscar/Downloads/opencv-master/release/modules/videostab/CMakeFiles/opencv_videostab_object.dir/src/deblurring.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/videostab/src/fast_marching.cpp" "/home/oscar/Downloads/opencv-master/release/modules/videostab/CMakeFiles/opencv_videostab_object.dir/src/fast_marching.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/videostab/src/frame_source.cpp" "/home/oscar/Downloads/opencv-master/release/modules/videostab/CMakeFiles/opencv_videostab_object.dir/src/frame_source.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/videostab/src/global_motion.cpp" "/home/oscar/Downloads/opencv-master/release/modules/videostab/CMakeFiles/opencv_videostab_object.dir/src/global_motion.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/videostab/src/inpainting.cpp" "/home/oscar/Downloads/opencv-master/release/modules/videostab/CMakeFiles/opencv_videostab_object.dir/src/inpainting.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/videostab/src/log.cpp" "/home/oscar/Downloads/opencv-master/release/modules/videostab/CMakeFiles/opencv_videostab_object.dir/src/log.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/videostab/src/motion_stabilizing.cpp" "/home/oscar/Downloads/opencv-master/release/modules/videostab/CMakeFiles/opencv_videostab_object.dir/src/motion_stabilizing.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/videostab/src/optical_flow.cpp" "/home/oscar/Downloads/opencv-master/release/modules/videostab/CMakeFiles/opencv_videostab_object.dir/src/optical_flow.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/videostab/src/outlier_rejection.cpp" "/home/oscar/Downloads/opencv-master/release/modules/videostab/CMakeFiles/opencv_videostab_object.dir/src/outlier_rejection.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/videostab/src/stabilizer.cpp" "/home/oscar/Downloads/opencv-master/release/modules/videostab/CMakeFiles/opencv_videostab_object.dir/src/stabilizer.cpp.o"
  "/home/oscar/Downloads/opencv-master/modules/videostab/src/wobble_suppression.cpp" "/home/oscar/Downloads/opencv-master/release/modules/videostab/CMakeFiles/opencv_videostab_object.dir/src/wobble_suppression.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "CVAPI_EXPORTS"
  "__OPENCV_BUILD=1"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../3rdparty/ippicv/unpack/ippicv_lnx/include"
  "."
  "../modules/videostab/include"
  "../modules/videostab/src"
  "modules/videostab"
  "../modules/core/include"
  "../modules/flann/include"
  "../modules/imgproc/include"
  "../modules/ml/include"
  "../modules/photo/include"
  "../modules/video/include"
  "../modules/imgcodecs/include"
  "../modules/videoio/include"
  "../modules/highgui/include"
  "../modules/features2d/include"
  "../modules/calib3d/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
